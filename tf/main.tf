terraform {
  required_version = ">= 0.12.0"
}

provider "azurerm" {
  features {}
  tenant_id = var.ARM_TENANT_ID
  subscription_id = var.ARM_SUBSCRIPTION_ID
  client_id = var.ARM_CLIENT_ID
  client_secret = var.ARM_CLIENT_SECRET
}

#resource "azurerm_virtual_network" "example" {
#  name                = "gitlab-network2"
#  resource_group_name = "kapil-arora"
#  location            = "WestEurope"
#  address_space       = ["10.0.0.0/16"]
#  tags = {
#    pipeline = "gitlab2"
#  }
#}

output "hello_world" {
  value = "Hello, World!"
}
